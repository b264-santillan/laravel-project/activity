@extends('layouts.app')

    @section('content')
        
        <center>
            <div class="container align-items-center align-content-center">
                <img src="../laravel.gif" width="200 px">
            </div>
        </center>

        
        <center>
            <h2 class="mb-3 mt-3">Featured Posts</h2>
        </center>

        @php 
            $counter = 0;
            $shuffledPosts = $posts->shuffle();          
        @endphp

        @foreach($shuffledPosts as $post)
           
            @if($counter < 3)
                <div class="card text-center mb-3">
                    <div class="card-body">
                        <h4 class="card-title mb-3"><a href="/posts/{{$post->id}}">{{$post->title}}</a></h4>

                        <h6 class="card-text mb-3">Author: {{$post->user->name}}</h6>

                    </div>
                </div>
            @endif
            @php $counter++; @endphp
        @endforeach

        <div class="mt-2">
            <center>
                <a class="btn btn-outline-primary" href="/posts">View all active posts</a>
                <a class="btn btn-outline-success" href="/myPosts">View all your posts</a>
                <a class="btn btn-success" href="/posts/create">Create new post</a>
            </center>
        </div>

    @endsection

