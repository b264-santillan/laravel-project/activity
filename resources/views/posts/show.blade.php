@extends('layouts.app')

@section('content')
	<div class="card">
		<div class="card-body">
			<h2 class="card-title">{{$post->title}} </h2>

			<p class="card-subtitle text-muted">Author: {{$post->user->name}} </p>
			<p class="card-subtitle text-muted mb-3">Created at: {{$post->created_at}} </p>
			<div class="card bg-info mx-5 py-3">
				<p class="card-text text-center">{{$post->content}} </p>
			</div>

			<p class="card-text text-left mt-3">Comments:</p>
			<div class="my-3">
				@foreach ($post->comments as $comment)
				           <li class="my-2 text-muted">{{ $comment->comment_content }}</li>
				@endforeach
			</div>

			@if(Auth::id() != $post->user_id)
			    <form class="d-inline" method="POST" action="/posts/{{$post->id}}/like">
			        @method('PUT')
			        @csrf
			        @if($post->likes->contains("user_id", Auth::id()))
			            <button type="submit" class="btn btn-danger">Unlike</button>
			        @else
			            <button type="submit" class="btn btn-success">Like</button>
			        @endif
			    </form>
			

				

				
				<!-- Button modal -->
				<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#commentModal">
				  Add Comment
				</button>

				<!-- Modal -->
				<div class="modal fade" id="commentModal" tabindex="-1" role="dialog" aria-labelledby="commentModalLabel" aria-hidden="true">
				  <div class="modal-dialog" role="document">
				    <div class="modal-content">
				      <div class="modal-header">
				        <h5 class="modal-title" id="commentModalLabel">Add Comment</h5>
				        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
				          <span aria-hidden="true">&times;</span>
				        </button>
				      </div>
				      <div class="modal-body">
				        <form method="POST" action="/posts/{{$post->id}}/comment">
				          @csrf
				          <div class="form-group">
				            <label for="content">Write your thoughts:</label>
				            <textarea class="form-control" id="content" name="content" rows="3"></textarea>
				          </div>
				          <div class="modal-footer">
				            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
				            <button type="submit" class="btn btn-primary">Post Comment</button>
				          </div>
				        </form>
				      </div>
				    </div>
				  </div>
				</div>
						

				

			@endif

			<div class="mt-3">
				<a class="btn btn-primary" href="/posts">View all active posts</a>
				<a class="btn btn-success text-light" href="/myPosts">View all posts</a>
			</div>


		</div>
	</div>
@endsection

