 @extends('layouts.app')

 @section('content')

 	@if (count($posts) > 0 )
    @foreach($posts as $post)
        @if ($post->isActive == 0)
            <div class="card text-center mb-3 {{ $post->isActive == 0 ? 'bg-dark' : 'bg-light' }}">
                <div class="card-body">
                    <h4 class="card-title mb-3"><a href="/posts/{{$post->id}}">{{$post->title}}</a></h4>
                    <h6 class="card-text mb-3">Author: {{$post->user->name}}</h6>
                    <p class="card-subtitle mb-3 text-muted">Created at: {{$post->created_at}}</p>
                </div>

                {{-- check if there is an authenticated user to prevent our web app from throwing an error when no user is logged in --}}
                @if(Auth::user())
                    @if(Auth::user()->id == $post->user_id)
                        <div>
                            <form method="POST" action="/posts/{{$post->id}}" class="mb-3">
                                <a href="/posts/{{$post->id}}/edit" class="btn btn-outline-primary">Edit Post</a>
                                
                                <form method="POST" action="/posts/{{$post->id}}">
                                    @csrf
                                    @method('PUT')
                                    <button type="submit" class="btn {{ $post->isActive == 0 ? 'btn-outline-success' : 'btn-outline-warning' }}">
                                        {{ $post->isActive == 0 ? 'Restore Post' : 'Archive Post' }}
                                    </button>
                                </form>

                                @method('DELETE')
                                @csrf
                                <button type="submit" class="btn btn-danger mb-3">Delete Post</button>
                            </form>
                        </div>
                    @endif
                @endif

            </div>
        @endif
    @endforeach
@endif


 @endsection