@extends('layouts.app')

@section('content')
	<form method="POST" action="/posts/{{$post->id}} ">
			@csrf
	        @method('PUT')

	        @if (Auth::user()->id == $post->user_id)
			<h1>EDIT YOUR POST</h1>
			<div class="form-group">
				<label for="title">
					Title:
				</label>
				<input type="text" name="title" class="form-control text-muted" id="title" value="{{ $post->title }}">
			</div>

			<div class="form-group">
				<label for="content">
					Content:
				</label>
				<textarea class="form-control text-muted" id="content" name="content" rows=3>{{ $post->content }}</textarea>
			</div>

			<div class="mt-2">
				<button type="submit" class="btn btn-success">Update Post</button>
			</div>

			@else 
			<h1 class="text-danger">You can not edit someone else's post!</h1>

			@endif
			
		</form>	


@endsection
