<?php

use Illuminate\Support\Facades\Route;

// for imports 
    // link for PostController file
    use App\Http\Controllers\PostController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

// :: Scope resolution operator

// acitivity new route for welcome page
Route::get('/', [PostController::class, 'welcome']);

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

// define a route wherein a view to create a post will be returned to the user
Route::get('/posts/create', [PostController::class, 'create']);

// define a route wherein the form data will be sent via POST method to the /posts URI endpoint
Route::post('/posts', [PostController::class, 'store']); 

// define a route that will retrun a view containing all posts
Route::get('/posts', [PostController::class, 'all']);

// define a route that will return a view containing only the authenticated user's posts.
Route::get('/myPosts', [PostController::class, 'myPosts']);


// define a route wherein a view showing a specific post with the matching url parameter id will be returned to the user (retrive a single post)
Route::get('/posts/{id}', [PostController::class, 'show']);

// activity 03-01
// define a route that will retrun an edit form for a specific Post when a GET request is received at posts/{id}/edit endpoint
Route::get('/posts/{id}/edit', [PostController::class, 'edit']);

// define a route that will overwrite an existing post with matching URL paramet ID via PUT method
Route::put('/posts/{id}',[PostController::class, 'update']);

//delete
Route::delete('/posts/{id}', [PostController::class, 'destroy']);

// activity 03-02
// archive
Route::put('/posts/{id}', [PostController::class, 'archive']); 

//archived posts
Route::get('/archives', [PostController::class, 'showArchive']);

// route to like a post
Route::put('/posts/{id}/like', [PostController::class, 'like']);

// route to comment to a post
Route::post('/posts/{id}/comment', [PostController::class, 'comment']);