<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    use HasFactory;

    // This defines that the post belongs to a specific user
    public function user()
        {
            return $this->belongsTo('App\Models\User');
        }

    // one to many relationship between Post model and PostLike model
    public function likes()
        {
            return $this->hasMany('App\Models\PostLike');
        }

    public function comments()
        {
            return $this->hasMany('App\Models\PostComment');
        }
}
