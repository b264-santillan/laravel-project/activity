<?php
 
namespace App\Http\Controllers;

use Illuminate\Http\Request;
// Auth (middleware) - access the authenticated user via auth facade.
use Illuminate\Support\Facades\Auth;
use App\Models\Post;
use App\Models\PostLike;
use App\Models\PostComment;

class PostController extends Controller
{
    // action to retrun a view/page containing a form for blog post creation
    public function create()
        {
            return view('posts.create');
        }

    public function store(Request $request)
        {
            if(Auth::user()) {
                // instantiate a new post object from the post model
                $post = new Post;

                // define the prooperties of the post object using the received form data.
                $post->title = $request->input('title');
                $post->content = $request->input('content');
                // get the ID of the authenticated user and set it as the foreign key user_id of the new post
                $post->user_id = (Auth::user()->id);

                // save this post object into the database
                $post->save();

                return redirect('/myPosts');
            }

            else {
                return redirect('/login');
            }
        }

    // action that will return a view showing all blog posts
     public function all()
        {
            $posts = Post::all();
            return view('posts.index')->with('posts', $posts);
        }

    // MINI ACTIVITY 03-01
        // refactor this to show only active posts
        public function index()
           {
                if(Auth::user()) {
                    $posts = Auth::user()->posts()->where('isActive', true)->get();
                    return view('posts.index')->with('posts', $posts);
                }
                else {
                    return redirect('/posts/create');
                }

              
           }

    // action for showing only the posts authored by the authenticated user    
    public function myPosts()
        {
            if(Auth::user()) {
                $posts = Auth::user()->posts;
                return view('posts.index')->with('posts', $posts);
            }
            else {
                return redirect('/login');
            }
            
        }

    public function welcome()
       {
           $posts = Post::all();

           /*
                Activity solution
                    $posts = Post::inRandomOrder()
                    -> limit(3)
                    -> get();
           */
           return view('welcome')->with('posts', $posts);
       }

    // action that will return a view showing a specific post using the URL parameter $id
    public function show($id) 
        {
            $post = Post::find($id);
            return view('posts.show')->with('post', $post);
        }

    // activity 03-01
    public function edit($id)
        {
            $post = Post::find($id);
            return view('posts.edit')->with('post', $post);
        }

    // action to update a specific post by an authenticated user
    public function update(Request $request, $id)
        {
            $post = Post::find($id);
            // if an authenticated user's ID is the same as the posts' user_id
            if (Auth::user()->id == $post->user_id) {
                $post->title= $request->input('title');
                $post->content = $request->input('content');
                $post->save();

                //return view('posts.show')->with('post', $post);

                return redirect('/posts/'.$id);
            }
            else {
                return redirect('/posts');
            }

            // other method
            // $post->title = $request->input('title');
            // $post->content = $request->input('content');
            // $post->save();
            
            // return redirect(route('posts.index'));
        }

    public function destroy($id)
        {
            $post = Post::find($id);

            if (Auth::user()->id === $post->user_id){
                $post->delete();
            }

            return redirect('/posts');
        }

    // activity 03-02
    public function archive($id)
        {
            $post = Post::find($id);

            if (Auth::user()->id == $post->user_id) {
                if($post->isActive) {
                    $post->isActive= 0;
                }
                else {
                    $post->isActive= 1;
                }
               
                $post->save();

                //return view('posts.show')->with('post', $post);

                return redirect('/myPosts');
            }
        }

    // show archived posts
    public function showArchive()
        {
            if(Auth::user()) {
                $posts = Auth::user()->posts;
                return view('posts.archives')->with('posts', $posts);
            }
            else {
                return redirect('/login');
            }
        }


    // LIKE FUNCTIONS

    public function like($id)
        {
            $post = Post::find($id);
            $user_id= Auth::user()->id;

            // if an authenticated user is not the post author
            if($post->user_id != $user_id) {
                // checks if a post like has been made by a certian user before
                if ($post->likes->contains("user_id", $user_id)) {
                    // delete the like made by this user to remove the like from this post
                    PostLike::where('post_id', $post->id)->where('user_id', $user_id)->delete();
                } else {
                    // create a new like record to like a specific post

                    // instantiate a new PostLike object from the PostLike model
                    $postLike = new PostLike;

                    // define the properties of the postlike object
                    $postLike->post_id = $post->id;
                    $postLike->user_id = $user_id;

                    // save this $postlike obj into the DB
                    $postLike->save();
                }
                // redirect the user back to the same post
                return redirect("/posts/$id");
            }

        }






    // COMMENT FUNCTIONS
    public function comment(Request $request, $id)
        {
            $post = Post::find($id);
            $user_id= Auth::user()->id;

            if($post->user_id = $user_id OR $post->user_id != $user_id) {
               
                    $postComment = new PostComment;


                    $postComment->comment_content = $request->input('content');
                    $postComment->post_id = $post->id;
                    $postComment->user_id = $user_id;

                    $postComment->save();
                

            return redirect("/posts/$id");
            }

        }


}




